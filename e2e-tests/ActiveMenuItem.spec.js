'use strict';

const expect = require('chai').expect;

describe('Active Menu', function() {

    beforeEach(function() {
        browser.get('index.html');
    });

    it('Page 1 click', function() {
        let link1 = element(by.css('[ui-sref="page1"]'));
        link1.click();
        expect(link1.getAttribute('class')).eventually.to.include('badge');
    });

    it('Page 2 click', function() {
        let link2 = element(by.css('[ui-sref="page2"]'));
        link2.click();
        expect(link2.getAttribute('class')).eventually.to.include('badge');
    });

    it('Page 3 click', function() {
        let link3 = element(by.css('[ui-sref="page3"]'));
        link3.click();
        expect(link3.getAttribute('class')).eventually.to.include('badge');
    });

});
