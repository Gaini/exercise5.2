'use strict';

const expect = require('chai').expect;

describe('Required Fields', function() {

    beforeEach(function() {
        browser.get('#!/myaccount');
    });

    var btn = element(by.tagName('button'));

    it('Name Entered - Button still disabled', function() {
        let nameField = element(by.id('accountName'));

        nameField.sendKeys('HumanOne');
        expect(btn.getAttribute('disabled')).eventually.to.equal('true');
    });

    it('Email Entered - Button still disabled', function() {
        let emailField = element(by.id('accountEmail'));

        emailField.sendKeys('MailOne@mail.com');
        expect(btn.getAttribute('disabled')).eventually.to.equal('true');
    });

    it('Name and Email both entered - Button Enabled', function() {
        let nameField = element(by.id('accountName')),
            emailField = element(by.id('accountEmail'));

        nameField.sendKeys('HumanOne');
        emailField.sendKeys('MailOne@mail.com');
        expect(btn.getAttribute('disabled')).eventually.to.equal(null);
    });


});
