'use strict';

const expect = require('chai').expect;

describe('MyAccount', function() {

    beforeEach(function() {
        browser.get('#!/myaccount');
    });

    it('MyAccount has form', function() {
        let form = element(by.tagName('form'));
        expect(form.getAttribute('class')).eventually.to.include('myaccount-form');
    });

});
